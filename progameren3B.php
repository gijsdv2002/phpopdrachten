<?php


class progameren3B
{
//    veranderd alle woorden Luftwaffe naar luftwaffel.
    public function luftwaffle(){
        $waffe = "The Luftwaffe[N 2] (German pronunciation: [ˈlʊftvafə] (About this soundlisten)) was the aerial warfare branch of the Wehrmacht during World War II. Germany's military air arms during World War I, the Luftstreitkräfte of the Imperial Army and the Marine-Fliegerabteilung of the Imperial Navy had been disbanded in May 1920 as a result of the terms of the Treaty of Versailles which stated that Germany was forbidden to have any air force.

During the interwar period, German pilots were trained secretly in violation of the treaty at Lipetsk Air Base in Soviet Union. With the rise of the Nazi Party and the repudiation of the Versailles Treaty, the Luftwaffe's existence was publicly acknowledged on 26 February 1935, just over two weeks before open defiance of the Versailles Treaty through German re-armament and conscription would be announced on 16 March.[9] The Condor Legion, a Luftwaffe detachment sent to aid Nationalist forces in the Spanish Civil War, provided the force with a valuable testing ground for new tactics and aircraft. Partially as a result of this combat experience, the Luftwaffe had become one of the most sophisticated, technologically advanced, and battle-experienced air forces in the world when World War II broke out in 1939.[10] By the summer of 1939, the Luftwaffe had twenty-eight Geschwader (wings). The Luftwaffe also operated Fallschirmjäger paratrooper units.

The Luftwaffe proved instrumental in the German victories across Poland and Western Europe in 1939 and 1940. During the Battle of Britain, however, despite inflicting severe damage to the RAF's infrastructure and, during the subsequent Blitz, devastating many British cities, the German air force failed to batter the beleaguered British into submission. From 1942, Allied bombing campaigns gradually destroyed the Luftwaffe's fighter arm. From late 1942, the Luftwaffe used its surplus ground support and other personnel to raise Luftwaffe Field Divisions. In addition to its service in the West, the Luftwaffe operated over the Soviet Union, North Africa and Southern Europe. Despite its belated use of advanced turbojet and rocket propelled aircraft for the destruction of Allied bombers, the Luftwaffe was overwhelmed by the Allies' superior numbers and improved tactics, and a lack of trained pilots and aviation fuel. In January 1945, during the closing stages of the Battle of the Bulge, the Luftwaffe made a last-ditch effort to win air superiority, and met with failure. With rapidly dwindling supplies of petroleum, oil, and lubricants after this campaign, and as part of the entire combined Wehrmacht military forces as a whole, the Luftwaffe ceased to be an effective fighting force.

After the defeat of Germany, the Luftwaffe was disbanded in 1946. During World War II, German pilots claimed roughly 70,000 aerial victories, while over 75,000 Luftwaffe aircraft were destroyed or significantly damaged. Of these, nearly 40,000 were lost entirely. The Luftwaffe had only two commanders-in-chief throughout its history: Hermann Göring and later Generalfeldmarschall Robert Ritter von Greim for the last two weeks of the war.

The Luftwaffe was deeply involved in Nazi war crimes. By the end of the war, a significant percentage of aircraft production originated in concentration camps, an industry employing tens of thousands of prisoners.[N 3] The Luftwaffe's demand for labor was one of the factors that led to the deportation and murder of hundreds of thousands of Hungarian Jews in 1944. The Oberkommando der Luftwaffe organized Nazi human experimentation, and Luftwaffe ground troops committed massacres in Italy, Greece, and Poland.";
        $waffel = str_replace('Luftwaffe','Luftwaffel', $waffe);

    return $waffel;
    }
}