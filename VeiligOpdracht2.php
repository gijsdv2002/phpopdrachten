<?php
// SESSION waarde kan worden aangepast door een ander tabblad of device te gebruiken
session_start();
$_SESSION['naam'] = get_current_user();
// POST kan worden aangepast door de gebruiker zelf door een nieuwe request te doen via een form
if (isset($_POST['post'])) {
    echo $_POST['post'];
}
// GET kan worden aangepast door de gebruiker zelf door een nieuwe request te doen door de url aan te passen of de form opnieuw in te vullen
if (isset($_GET['get'])) {
    echo $_GET['get'];
}
echo '<br>SESSION<br>';
var_dump($_SESSION['naam']);
// COOKIE kan worden aangepast door een andere computer te gebruiken en de cookie te verwijderen en opnieuw op de website te gaan
setcookie('naam', get_current_user());
echo '<br>COOKIE<br>';
var_dump($_COOKIE['naam']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Opdracht 2 </title>
</head>
<body>
<form name="Table" method="post">

    <input type="text" name="post" required placeholder="POST">
    <input type="submit" name="verstuur1" value="Verstuur">
</form>
<form name="Table" method="get">

    <input type="text" name="get" required placeholder="GET">
    <input type="submit" name="verstuur2" value="Verstuur">
</form>

</body>
</html>







