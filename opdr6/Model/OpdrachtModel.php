<?php


class OpdrachtModel
{
//    haalt data op
    private $db;
    private $data;
    private $hind;


    public function setHind($hind)
    {
        if ($hind == 1 || 2) {
            $this->hind = $hind;
        }
        return false;
    }


    public function getData()
    {
        $this->fetch();
        if ($this->data !== null) {
            return $this->data;
        }
        return false;
    }




    public function __construct()
    {
        $user = 'root';
        $pass = '';
        try {
            // database connectie
            $db = new PDO('mysql:host=localhost;dbname=hindenburg', $user, $pass);
            return $this->db = $db;
        }
        // foutmelding als database niet bestaat
        catch (PDOException $e) {
         return $this->db = 'sorry voor het ongemak de verbinding met de server is verbroken probeer het later nog eens';
        }
    }

    private function fetch()
    {
        try {
            $query = "SELECT * FROM photo WHERE photo.id = (:hind);";
            $pre = $this->db->prepare($query);
            $pre->bindParam(':hind', $this->hind);
            $pre->execute();
            return $this->data = $pre->fetchAll();
        }
            // foutmelding als tabel niet bestaat
        catch (PDOException $e) {
                return $this->db = 'sorry voor het ongemak de verbinding met de server is verbroken probeer het later nog eens';
            }



    }
}