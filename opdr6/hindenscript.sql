create table if not exists photo
(
    id   int auto_increment
        primary key,
    img  varchar(255) null,
    text text         null
);
INSERT INTO hindenburg.photo (id, img, text) VALUES (1, 'https://upload.wikimedia.org/wikipedia/commons/1/1c/Hindenburg_disaster.jpg', 'LZ 129 Hindenburg (Luftschiff Zeppelin #129; Registration: D-LZ 129) was a large German commercial passenger-carrying rigid airship, the lead ship of the Hindenburg class, the longest class of flying machine and the largest airship by envelope volume.[3] It was designed and built by the Zeppelin Company (Luftschiffbau Zeppelin GmbH) on the shores of Lake Constance in Friedrichshafen, Germany, and was operated by the German Zeppelin Airline Company (Deutsche Zeppelin-Reederei). The airship flew from March 1936 until it was destroyed by fire 14 months later on May 6, 1937 while attempting to land at Lakehurst Naval Air Station in Manchester Township, New Jersey, at the end of the first North American transatlantic journey of its second season of service with the loss of 36 lives. This was the last of the great airship disasters; it was preceded by the crashes of the British R38 in 1921 (44 dead), the US airship Roma in 1922 (34 dead), the French Dixmude in 1923 (52 dead), the British R101 in 1930 (48 dead), and the USS Akron in 1933 (73 dead).

Hindenburg was named after the late Field Marshal Paul von Hindenburg, President of Germany from 1925 until his death in 1934.');
INSERT INTO hindenburg.photo (id, img, text) VALUES (2, 'https://upload.wikimedia.org/wikipedia/commons/1/1b/Bundesarchiv_Bild_183-C06886%2C_Paul_v._Hindenburg.jpg', 'Paul Ludwig Hans Anton von Beneckendorff und von Hindenburg (About this soundlisten), known simply as Paul von Hindenburg (German: [ˈpaʊl fɔn ˈhɪndn̩bʊɐ̯k] (About this soundlisten); 2 October 1847 – 2 August 1934), was a German general and statesman who commanded the Imperial German Army during World War I and later became President of Germany from 1925 until his death, during the Weimar Republic. He played a key role in the Nazi Machtergreifung in January 1933 when, under pressure from advisers, he appointed Adolf Hitler Chancellor of Germany.');


