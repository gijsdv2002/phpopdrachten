<?php
// bepaald de header
if (isset($_GET['head'])) {
    switch ($_GET['head']) {
        case '2':
            include('headers/GermanHead.php');
            break;
        case '1':
            include('headers/RussianHead.php');
            break;
    }}
else{
    //start header
    include('headers/BeginHead.php');
}
// bepaald de body
if (isset($_GET['body'])) {
    switch ($_GET['body']) {
        case '1':
            include('content/VideoBody.php');
            break;
        case '2':
            include('content/TekstBody.php');
            break;
    }}
else{
    //start body
    include('content/BeginContent.php');
}
// bepaald de footer
if (isset($_GET['foot'])) {
    switch ($_GET['foot']) {
        case '1':
            include('footers/FooterKleur.php');
            break;
        case '2':
            include('footers/FooterEigenaar.php');
            break;
    }}
else{
    //start footer
    include('footers/BeginFooter.php');
}
