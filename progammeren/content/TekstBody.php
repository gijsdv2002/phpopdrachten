<p>
<h2>Ancient history</h2>
<br><Br>
Roman public toilets, Ostia Antica.
<br><Br>
Model of toilet with pigsty, China, Eastern Han dynasty 25 – 220 AD
During the third millennium BC, toilets and sewers were invented. The Indus Valley Civilisation in northwestern
India and Pakistan was home to the world's first known urban sanitation systems. In Mohenjo-Daro (c. 2800 BC),
toilets were built into the outer walls of homes. These toilets had vertical chutes, via which waste was disposed of
into cesspits or street drains.[26] Another typical example is the Indus city of Lothal (c. 2350 BCE). In Lothal all
houses had their own private toilet which was connected to a covered sewer network constructed of brickwork held
together with a gypsum-based mortar that emptied either into the surrounding water bodies or alternatively into
cesspits, the latter of which were regularly emptied and cleaned.[27][28]
<br><Br>
The Indus Valley Civilization also had water-cleaning toilets that used flowing water in each house that were linked
with drains covered with burnt clay bricks. The flowing water removed the human waste.[citation needed] The Indus
Valley civilisation had a network of sewers built under grid pattern streets.[citation needed]
<br><Br>
Other very early toilets that used flowing water to remove the waste are found at Skara Brae in Orkney, Scotland,
which was occupied from about 3100 BC until 2500 BC. Some of the houses there have a drain running directly beneath
them, and some of these had a cubicle over the drain. Around the 18th century BC, toilets started to appear in
Minoan Crete, Pharaonic Egypt, and ancient Persia.
<br><Br>
In 2012, archaeologists found what is believed to be Southeast Asia's earliest latrine during the excavation of a
neolithic village in the Rạch Núi archaeological site, southern Vietnam. The toilet, dating back 1500 BC, yielded
important clues about early Southeast Asian society. More than 30 coprolites, containing fish and shattered animal
bones, provided information on the diet of humans and dogs, and on the types of parasites each had to contend
with.[29][30][31]
<br><Br>
In Roman civilization, toilets using flowing water were sometimes part of public bath houses. Roman toilets, like
the ones pictured here, are commonly thought to have been used in the sitting position. The Roman toilets were
probably elevated to raise them above open sewers which were periodically "flushed" with flowing water, rather than
elevated for sitting. Romans and Greeks also used chamber pots, which they brought to meals and drinking
sessions.[32] Johan J. Mattelaer said, "Plinius has described how there were large receptacles in the streets of
cities such as Rome and Pompeii into which chamber pots of urine were emptied. The urine was then collected by
fullers." (Fulling was a vital step in textile manufacture.)
<br><Br>
The Han dynasty in China two thousand years ago used pig toilets.
<br><Br>
<h2> Post-classical history</h2>
Garderobes were toilets used in the Post-classical history, most commonly found in upper-class dwellings.
Essentially, they were flat pieces of wood or stone spanning from one wall to the other, with one or more holes to
sit on. These were above chutes or pipes that discharged outside the castle or Manor house.[33] Garderobes would be
placed in areas away from bedrooms to shun the smell[34] and also near kitchens or fireplaces to keep the enclosure
warm.[33]

<br><br><Br>
Garderobe seat openings

<br><Br><br>
View looking down into garderobe seat opening
<br><Br><br>

Exterior view of garderobe at Campen castle

<br><Br><br>
Toilet in Rosenborg Castle Copenhagen
<br><Br>
The other main way of handling toilet needs was the chamber pot, a receptacle, usually of ceramic or metal, into
which one would excrete waste. This method was used for hundreds of years; shapes, sizes, and decorative variations
changed throughout the centuries.[35] Chamber pots were in common use in Europe from ancient times, even being taken
to the Middle East by medieval pilgrims.[36]
<br><Br>
<h2>Modern history</h2>
<br><Br>
Bourdaloue chamber pots from the Austrian Imperial household
<br><Br>
Early 18th century British three-seat privy
<br><Br>
19th century thunderbox, a heavy wooden commode to enclose chamber pot
By the Early Modern era, chamber pots were frequently made of china or copper and could include elaborate
decoration. They were emptied into the gutter of the street nearest to the home.
<br><Br>
In pre-modern Denmark, people generally defecated on farmland or other places where the human waste could be
collected as fertilizer.[37] The Old Norse language had several terms for referring to outhouses, including garðhús
(yard house), náð-/náða-hús (house of rest), and annat hús (the other house). In general, toilets were functionally
non-existent in rural Denmark until the 18th century.[37]
<br><Br>
By the 16th century, cesspits and cesspools were increasingly dug into the ground near houses in Europe as a means
of collecting waste, as urban populations grew and street gutters became blocked with the larger volume of human
waste. Rain was no longer sufficient to wash away waste from the gutters. A pipe connected the latrine to the
cesspool, and sometimes a small amount of water washed waste through. Cesspools were cleaned out by tradesmen, known
in English as gong farmers, who pumped out liquid waste, then shovelled out the solid waste and collected it during
the night. This solid waste, euphemistically known as nightsoil, was sold as fertilizer for agricultural production
(similarly to the closing-the-loop approach of ecological sanitation).
<br><Br>
The garderobe was replaced by the privy midden and pail closet in early industrial Europe.[citation needed]
<br><Br>
In the early 19th century, public officials and public hygiene experts studied and debated sanitation for several
decades. The construction of an underground network of pipes to carry away solid and liquid waste was only begun in
the mid 19th-century, gradually replacing the cesspool system, although cesspools were still in use in some parts of
Paris into the 20th century.[38] Even London, at that time the world's largest city, did not require indoor toilets
in its building codes until after the First World War.
<br><Br>
The water closet, with its origins in Tudor times, started to assume its currently known form, with an overhead
cistern, s-bends, soil pipes and valves around 1770. This was the work of Alexander Cumming and Joseph Bramah. Water
closets only started to be moved from outside to inside of the home around 1850.[39] The integral water closet
started to be built into middle-class homes in the 1860s and 1870s, firstly on the principal bedroom floor and in
larger houses in the maids' accommodation, and by 1900 a further one in the hallway. A toilet would also be placed
outside the back door of the kitchen for use by gardeners and other outside staff such as those working with the
horses. The speed of introduction was varied, so that in 1906 the predominantly working class town of Rochdale had
750 water closets for a population of 10,000.[39]
<br><Br>
The working-class home had transitioned from the rural cottage, to the urban back-to-back terraces with external
rows of privies, to the through terraced houses of the 1880 with their sculleries and individual external WC. It was
the Tudor Walters Report of 1918 that recommended that semi-skilled workers should be housed in suburban cottages
with kitchens and internal WC. As recommended floor standards waxed and waned in the building standards and codes,
the bathroom with a water closet and later the low-level suite, became more prominent in the home.[40]
<br><Br>
Before the introduction of indoor toilets, it was common to use the chamber pot under one's bed at night and then to
dispose of its contents in the morning. During the Victorian era, British housemaids collected all of the
household's chamber pots and carried them to a room known as the housemaids' cupboard. This room contained a "slop
sink", made of wood with a lead lining to prevent chipping china chamber pots, for washing the "bedroom ware" or
"chamber utensils". Once running water and flush toilets were plumbed into British houses, servants were sometimes
given their own lavatory downstairs, separate from the family lavatory.[41] The practice of emptying one's own
chamber pot, known as slopping out, continued in British prisons until as recently as 2014[42] and was still in use
in 85 cells in the Republic of Ireland in July 2017.[43]
<br><Br>
With rare exceptions, chamber pots are no longer used. Modern related implements are bedpans and commodes, used in
hospitals and the homes of invalids.
<br><Br>
Development of dry earth closets
Further information: Dry toilet § History
<br><Br>
Henry Moule's earth closet design, circa 1909
Before the widespread adoption of the flush toilet, there were inventors, scientists, and public health officials
who supported the use of "dry earth closets" - nowadays known either as dry toilets or composting toilets.[44]
<br><Br>
Development of flush toilets
Further information: Flush toilet § History
Although a precursor to the flush toilet system which is widely used nowadays was designed in 1596 by John
Harington,[citation needed] such systems did not come into widespread use until the late nineteenth
century.[citation needed] With the onset of the industrial revolution and related advances in technology, the flush
toilet began to emerge into its modern form. A crucial advance in plumbing, was the S-trap, invented by the Scottish
mechanic Alexander Cummings in 1775, and still in use today. This device uses the standing water to seal the outlet
of the bowl, preventing the escape of foul air from the sewer. It was only in the mid-19th century, with growing
levels of urbanisation and industrial prosperity, that the flush toilet became a widely used and marketed invention.
This period coincided with the dramatic growth in the sewage system, especially in London, which made the flush
toilet particularly attractive for health and sanitation reasons.[39]
<br><Br>
Flush toilets were also known as "water closets", as opposed to the earth closets described above. WCs first
appeared in Britain in the 1880s, and soon spread to Continental Europe. In America, the chain-pull indoor toilet
was introduced in the homes of the wealthy and in hotels in the 1890s. William Elvis Sloan invented the Flushometer
in 1906, which used pressurized water directly from the supply line for faster recycle time between flushes.
<br><Br>
High-tech toilet
See also: Toilets in Japan
"High-tech" toilets, which can be found in countries like Japan, include features such as automatic-flushing
mechanisms; water jets or "bottom washers"; blow dryers, or artificial flush sounds to mask noises. Others include
medical monitoring features such as urine and stool analysis and the checking of blood pressure, temperature, and
blood sugar. Some toilets have automatic lid operation, heated seats, deodorizing fans, or automated replacement of
paper toilet-seat-covers. Interactive urinals have been developed in several countries, allowing users to play video
games. The "Toylet", produced by Sega, uses pressure sensors to detect the flow of urine and translates that into
on-screen action.[45]
<br><Br>
Astronauts on the International Space Station use a space toilet with urine diversion which can recover potable
water.[46]
<br><Br>
Names
See also: Toilet (room) § Names, and Outhouse § Names
<br><Br>
Etymology
<br><Br>
In La Toilette from Hogarth's Marriage à la Mode series (1743), a young countess receives her lover, tradesmen,
hangers-on, and an Italian tenor as she finishes her toilette[47]
<br><Br>
Detail of Queen Charlotte with her Two Eldest Sons, Johan Zoffany, 1765, (the whole painting). She is doing her
toilet, with her silver-gilt toilet service on the dressing-table
Toilet was originally a French loanword (first attested in 1540) that referred to the toilette ("little cloth")
draped over one's shoulders during hairdressing.[48] During the late 17th century,[48] the term came to be used by
metonymy in both languages for the whole complex of grooming and body care that centered at a dressing table (also
covered by a cloth) and for the equipment composing a toilet service, including a mirror, hairbrushes, and
containers for powder and makeup. The time spent at such a table also came to be known as one's "toilet"; it came to
be a period during which close friends or tradesmen were received as "toilet-calls".[48][51]
<br><Br>
The use of "toilet" to describe a special room for grooming came much later (first attested in 1819), following the
French cabinet de toilet. Similar to "powder room", "toilet" then came to be used as a euphemism for rooms dedicated
to urination and defecation, particularly in the context of signs for public toilets, as on trains. Finally, it came
to be used for the plumbing fixtures in such rooms (apparently first in the United States) as these replaced chamber
pots, outhouses, and latrines. These two uses, the fixture and the room, completely supplanted the other senses of
the word during the 20th century[48] except in the form "toiletries".[n 2]
<br><Br>
Contemporary use
The word "toilet" was by etymology a euphemism, but is no longer understood as such. As old euphemisms have become
the standard term, they have been progressively replaced by newer ones, an example of the euphemism treadmill at
work.[52] The choice of word relies not only on regional variation, but also on social situation and level of
formality (register) or social class. American manufacturers show an uneasiness with the word and its class
attributes: American Standard, the largest firm, sells them as "toilets", yet the higher priced products of the
Kohler Company, often installed in more expensive housing, are sold as commodes or closets, words which also carry
other meanings. Confusingly, products imported from Japan such as TOTO are referred to as "toilets", even though
they carry the cachet of higher cost and quality. (Toto, an abbreviation of Tōyō Tōki (東洋陶器 Oriental Ceramics), is
used in Japanese comics to visually indicate toilets or other things that look like toilets; see Toilets in Japan.)
<br><Br>
Regional variants
Different dialects use "bathroom" and "restroom" (American English), "bathroom" and "washroom" (Canadian English),
and "WC" (an initialism for "water closet"), "lavatory" and its abbreviation "lav" (British English). Euphemisms for
the toilet that bear no direct reference to the activities of urination and defecation are ubiquitous in modern
Western languages, reflecting a general attitude of unspeakability about such bodily function.[citation needed]
These euphemistic practices appear to have become pronounced following the emergence of European colonial practices,
which frequently denigrated colonial subjects in Africa, Asia and South America as 'unclean'.[53][54]
<br><Br>
Euphemisms
"Crapper" was already in use[citation needed] as a coarse name for a toilet, but it gained currency from the work of
Thomas Crapper, who popularized flush toilets in England.
<br><Br>
"The Jacks" is Irish slang for toilet.[55] It perhaps derives from "jacques" and "jakes", an old English term.[56]
<br><Br>
"Loo" – The etymology of loo is obscure. The Oxford English Dictionary notes the 1922 appearance of "How much cost?
Waterloo. Watercloset." in James Joyce's novel Ulysses and defers to Alan S. C. Ross's arguments that it derived in
some fashion from the site of Napoleon's 1815 defeat.[57][58] In the 1950s the use of the word "loo" was considered
one of the markers of British upper-class speech, featuring in a famous essay, "U and non-U English".[59] "Loo" may
have derived from a corruption of French l'eau ("water"), gare à l'eau ("mind the water", used in reference to
emptying chamber pots into the street from an upper-story window), lieu ("place"), lieu d'aisance ("place of ease",
used euphemistically for a toilet), or lieu à l'anglaise ("English place", used from around 1770 to refer to
English-style toilets installed for travelers).[57][60][61] Other proposed etymologies include a supposed tendency
to place toilets in room 100 (hence "loo") in English hotels,[62] a dialectical corruption of the nautical term
"lee" in reference to the need to urinate and defecate with the wind prior to the advent of head pumps,[n 3] or the
17th-century preacher Louis Bourdaloue, whose long sermons at Paris's Saint-Paul-Saint-Louis prompted his
parishioners to bring along chamber pots.[63]
</p>
<br>
<!--terug naar het begin-->
<button><a href="http://localhost/PHPOpdrachten/progammeren/">terug</a></button>